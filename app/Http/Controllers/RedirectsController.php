<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RedirectsController extends Controller
{
  public function __invoke() {

    $redirect = new Redirect;
    $json_redirects = file_get_contents('BASE_URL/wp-json/test-redirects/1');
    $json_redirects = json_decode( $json_redirects );

    if( is_array( $json_redirects ) ) {
      foreach( $json_redirects as $json_redirect ) {
        $redirect->url = $json_redirect['url'];
        $redirect->save();
      }
    } 
  }
}
